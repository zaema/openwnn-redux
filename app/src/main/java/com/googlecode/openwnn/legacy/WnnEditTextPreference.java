/*
 * Copyright (C) 2020, zaema
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.googlecode.openwnn.legacy;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class WnnEditTextPreference extends EditTextPreference {
    public WnnEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Redux: Always append the value of the candidate view height to the title when it is drawn
     */
    @Override
    protected View onCreateView(ViewGroup parent) {
        setTitle("Candidate View Height:  " + getText());
        // Redux TODO: obtain the title from the string resource (R.string.preference_candidate_view_height_title only gets the integer id)
        return super.onCreateView(parent);
    }

    /**
     * Redux: Update the title to include the value when it is changed
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        setTitle("Candidate View Height:  " + getText());
        // Redux TODO: see above
    }
}
