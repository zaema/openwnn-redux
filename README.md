# OpenWnn Redux
OpenWnn Redux is a fork of the [OpenWnn Legacy project](https://code.google.com/archive/p/openwnn-legacy), with the aim of fixing critical usage bugs and implementing quality-of-life features.

## Building
Currently, there is no CI/CD pipeline, as I am still trying to figure out how to make gradle compile OpenWnn's native c libraries.

However, a debug apk can be built using Android Studio, or IntelliJ IDEA with the Android Studio extension.

## Roadmap
OpenWnn Redux is still a work in progress.
- [X] Fix flickering that occurs on some devices when updating the candidates view that may be caused by the candidates view being drawn with insufficient height
  - A bit of a hack fix that provides the user with a setting to increase the height of the candidates view until it no longer flickers 
- [ ] Configure the building of native libraries using gradle
- [ ] Refactor package name
- [ ] Implement option to change key height

## Copyright
Copyright (C) 2020 zaema

OpenWnn Redux is licenced under the [Apache 2.0 License](LICENSE).

